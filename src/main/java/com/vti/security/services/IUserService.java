package com.vti.security.services;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.vti.entity.User;

public interface IUserService extends UserDetailsService {
	public Page<User> getAllAccounts(Pageable pageable, String search);

	public User getAccountByID(int id);

	public User findByUsername(String username);

	UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

	User findUserByEmail(String email);

	User findUserByUsername(String username);

	void activeUser(String token);

	void sendConfirmUserRegistrationViaEmail(String email);

	boolean existsUserByEmail(String email);

	boolean existsUserByUsername(String userName);

	void resetPasswordViaEmail(String email);

	void resetPassword(String token, String newPassword);

	void sendResetPasswordViaEmail(String email);

	public void upBlockExpDate(int id, Integer date);

	public void unBlockExpDate(int id);

	ByteArrayResource getAvatar(int id);

	void registerUser(User user);

}
