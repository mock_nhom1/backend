package com.vti.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Film", uniqueConstraints = { @UniqueConstraint(columnNames = "title"),
        @UniqueConstraint(columnNames = "vn_title") })
public class Film {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    @NotBlank
    @Size(max = 150)
    private String title;

    @Column(name = "vn_title")
    @NotBlank
    @Size(max = 150)
    private String vnTitle;

    @Lob
    @Column(name="story_line", length=512)
    private String content;

    @Column(name = "url_avatar", length = 100)
    private String urlAvatar;

    @NotBlank
    @Size(max = 120)
    private String password;

    @Column
    private Date blockExpDate; // mốc thời gian user bị khoá

    @ManyToMany(fetch = FetchType.LAZY,cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "account_roles",
            joinColumns = { @JoinColumn(name = "account_ID") },
            inverseJoinColumns = { @JoinColumn(name = "role_ID") })
    private List<Role> roles;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`status`", nullable = false)
    private Status status = Status.ACTIVE;

}
