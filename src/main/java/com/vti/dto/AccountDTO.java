package com.vti.dto;

import java.util.List;

import com.vti.entity.Role;
import com.vti.entity.Status;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//chuyển đổi giữa dữ liệu lấy được từ DB đẩy lên Frontend.
public class AccountDTO {

	private int id;
	private String email;
	private String username;
	private List<Role> role;
	private Status status;

}
