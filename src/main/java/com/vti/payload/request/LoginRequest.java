package com.vti.payload.request;

import java.util.List;

import javax.validation.constraints.NotBlank;

import com.vti.entity.Status;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {
	@NotBlank
	private String username;

	@NotBlank
	private String password;

	private String email;

	private List<String> role;

	private Status status;

}
